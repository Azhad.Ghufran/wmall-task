const express = require('express');
const app = express();
const PORT = 8080;
app.get('/', (req, res) => {
    res.send("Welcome to the main Page");

})
app.get('/home', (req, res) => {
    res.send("Welcome to the Home page");

})
app.get('/about', (req, res) => {
    res.send("Welcome to the about Page");

})
app.get('/Contact', (req, res) => {
    res.send("Welcome to the Contact Page");

})
app.listen(PORT);