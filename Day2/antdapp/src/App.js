import { DatePicker, message,Alert } from 'antd';
import React,{useState} from 'react';
import 'antd/dist/antd.css';
function App() {
  const [Date,setDate] = useState("");
  const onChangeHandle = val =>{
    setDate(val);
  }
  return (
    <>
    <h1> Hi this is antd app</h1>
    <div style={{display:"flex",flexDirection:"column",justifyContent:"space-around",background:"green",width:"400px",height:"400px",margin:"100px auto"}}>
      <DatePicker onChange = {onChangeHandle}/>
      <div style={{marginTop:"16px"}}>
        <Alert message="Selected Date" description = {Date ? Date.format('YYYY-MM-DD') : 'None'} />
      </div>
    </div>
    </>
  );
}

export default App;
