import React, { useState,useEffect } from "react";
import "./App.css";
import { Button,List,Input,Modal } from 'antd';
import "antd/dist/antd.css";
import axios from "axios";
import Todo from "./components/Todo";
import EditTodo from "./components/EditTodo";
function App() {
  const [userinput, setuserinput] = useState("");
  const [editUserInput,setEditUserInput] = useState("");
  const [todolist, setTodolist] = useState([]);
  const [editTodolist,setEditTodoList] = useState("");
  const [flag,setFlag] = useState(true);
  const { TextArea } = Input;

  // Modal Visibilty variable 
  const [isModalVisible, setIsModalVisible] = useState(false);

  const baseURL = "http://localhost:5000/api/items/";
  useEffect(() => {
    axios.get("http://localhost:5000/api/items/",{
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
      }).then(res => res.data).then(res => {
        {
          setTodolist(res);
        }
      });
  },[flag])
  

  const onEditInputChange = (event) => {
    setEditUserInput(event.target.value);
  };
  const onInputChange = (event) => {
    setuserinput(event.target.value);
  };
  const onaddTodo = () => {
    axios.post("http://localhost:5000/api/items/",{"name" : userinput})
      .then(res => console.log("data posted"))
    setTodolist((prevValue) => {
      return [...prevValue, userinput];
    });
    setuserinput("");
    setFlag(!flag);
  };
  const edithandler = (value) =>{
    setEditTodoList(value);
    // setFlag(!flag);
  }
  const saveHandler = (value) =>{
    console.log(value._id);
    const putUrl = baseURL + value._id;
    axios.put(putUrl,{"name" : editUserInput,"Date" : "2021-10-28T14:26:25.352Z"}).then(res => console.log("Value Updated"))
    setFlag(!flag);
  }
  const onDeleteTodoHandler = (value) => {
    // how to get value and id from database 
    // and then delete the files with the relevant id

		const newtodolist = [...todolist].filter(item => item === value)
    const itemId = newtodolist[0]._id;
    let deleteURL = baseURL + "/" + itemId;
    axios.delete(deleteURL)
      .then(() => setFlag(!flag))
    
	}
  const getTodolist = () => {
    const todos = todolist.map((elem, idx) => (
      <li
        className="list"
        key={idx + 1}
        style={{
          backgroundColor: "#ff6666",
          margin: "1rem",
          padding: "0.5rem",
		  display:"flex",
      flexDirection:"row",
      color:"white",
      fontWeight:"bold",
      fontSize:"20px",
		  justifyContent:"space-between"
        }}
      >
        {editTodolist===elem.name ? 
        <EditTodo elem = {elem} editUserInput = {editUserInput} onEditInputChange = {onEditInputChange} saveHandler = {saveHandler}/>
        : <Todo elem = {elem}  deleteHandle = {onDeleteTodoHandler} edithandler = {edithandler} />}
        
      </li>
    ));
	
    return <ul>{todos}</ul>;
  };

  // Modal handler  
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    if (userinput.length)
    {
      onaddTodo();
    }  
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  ////////////

  return (
    <div id="main" style={{ padding:"10px",margin:"50px",backgroundColor:"white",height:"530px"}}>
      <div className="header">
      <h1 style={{ textAlign:"center",fontFamily:"inter",color:"#ff6666",fontWeight:"bold"}}> Todo App </h1>
      <Button type="primary" onClick={showModal} style = {{backgroundColor:"white",color:"#ff6666",fontWeight:"bold",border:"1px solid #ff6666"}}>
        Click to Add task
      </Button>
      </div>
      
      {isModalVisible ? <Modal title="Add Task" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} okText = "Add Todo">
        <TextArea id="task" value={userinput} autoSize showCount maxLength={100} onChange={onInputChange} />
        </Modal> : ""}
      <div className={`todo-info box`} style={{ fontFamily:"inter",marginTop:"15px",marginLeft:"150px",height:"400px",width:"800px",backgroundColor:"#ffcccc",overflow:"auto",border:"2px solid white",borderRadius:"5px"}}>
      {getTodolist()}
      </div>
    </div>
  );
}

export default App;