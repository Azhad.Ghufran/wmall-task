import React from "react";
function EditTodo(props)
{
    const { elem,editUserInput ,onEditInputChange,saveHandler } = props;
    return(
        <React.Fragment>
          <textarea className="editTask" value={editUserInput} onChange = {onEditInputChange} style={{ paddingLeft:"15px",color:"black",background:"white" }} />
          <span>
          <button className="saveTask" disabled = {!editUserInput} onClick = {() => {saveHandler(elem)}} style={{ paddingLeft:"15px",color:"black",background:"white",color:"#ff6666",border:"none",borderRadius:"5px" }} > Save </button>
        </span>
        </React.Fragment>
    );
}
export default EditTodo;