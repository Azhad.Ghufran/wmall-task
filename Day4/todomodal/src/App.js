import React, { useState } from "react";
import "antd/dist/antd.css";
import "./index.css";
import { Modal, Button, Input } from "antd";
const { TextArea } = Input;

const App = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Open Modal
      </Button>
      <Modal
        title="Basic Modal"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Set Todo</p>
        <TextArea placeholder="Set todo" />
        <p>Some contents...</p>
      </Modal>
    </>
  );
};

export default App;
