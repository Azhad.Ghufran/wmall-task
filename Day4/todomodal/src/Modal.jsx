function Modal()
{
    return (
        <Modal
        title="Basic Modal"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Set Todo</p>
        <TextArea placeholder="Set todo" />
        <p>Some contents...</p>
      </Modal>
    );
}
export default Modal;