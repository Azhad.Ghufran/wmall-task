import React from "react";
function Todo(props)
{
    const { elem,deleteHandle,edithandler } = props;
    
    return(
        <React.Fragment>
          <span> {elem.name} </span>
        <span>
          <button className = "edit" onClick = {() => {edithandler(elem.name)}} style={{ paddingLeft:"15px",color:"black",background:"white",color:"#ff6666",border:"none",borderRadius:"5px",marginRight:"3px" }}> Edit </button>
          <button className="delete" onClick = {() => deleteHandle(elem)} style={{ paddingLeft:"15px",color:"black",background:"white",color:"#ff6666",border:"none",borderRadius:"5px",marginLeft:"3px" }}> Delete </button>
        </span>
        </React.Fragment>
    );
}
export default Todo;