const express = require('express');
const router = express.Router();

// Item model
const Item = require('../../models/Item');

// route GET api/items
router.get('/', (req, res) => {
    Item.find().sort({ date: -1 }).then(items => res.json(items));
});

// post api , add data to database 
router.post('/', (req, res) => {
    const newItem = new Item({
        name: req.body.name
    });
    newItem.save().then((item) => res.json(item));
});
// delete api delete data from database 

router.delete('/:id', (req, res) => {
    Item.findById(req.params.id)
        .then(item => item.remove().then(() => res.json({ success: true })))
        .catch(err => res.status(400).json({ success: false }));
});

// update api delete data from database 

router.put('/:id', (req, res) => {
    Item.findByIdAndUpdate(req.params.id, req.body)
        .then(item => res.json({ msg: 'Updated successfully' }))
        .catch(err =>
            res.status(400).json({ error: 'Unable to update the Database' })
        );
});

module.exports = router;