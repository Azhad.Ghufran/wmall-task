const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require("cors");
const app = express();

const items = require('./routes/api/items');
// 
app.use(bodyParser.json());
app.use(cors());

// Db config
const db = require('./config/keys').mongoURI;
// connect to mongodb
mongoose
    .connect(db)
    .then(() => console.log("Mongodb is connected"))
    .catch((err) => console.log(err));

app.use('/api/items', items);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Server started on Port ${PORT}`));